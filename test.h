#ifndef TEST_H
#define TEST_H

#include <QObject>
#include <QDebug>
#include <QVariant>
#include <QRandomGenerator>

class Test : public QObject
{
    Q_OBJECT
public:
    explicit Test(QObject *parent = nullptr);

signals:
    void status(QVariant data);


public slots:
    void work();
    void addValues( int id, QVariant surgeon_id, int dia_power_max,int dia_power_mode,int cortex_asp_max,int cortex_asp_mode , int  cortex_vac_max , int cortex_vac_mode , int polish_asp_max , int polish_asp_mode , int polish_vac_max ,int polish_vac_mode , int vit_cutrate_max , int vit_cutrate_mode , int vit_asp_max , int vit_asp_mode , int vic_vac_max , int vit_vac_mode ,int fs_top_left , int fs_top_right ,int fs_bottom_left ,int fs_bottom_right);
    void createdatabase();

};

#endif // TEST_H
