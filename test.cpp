#include "test.h"
#include<QtSql>
#include<QDebug>
Test::Test(QObject *parent) : QObject(parent)
{

}

void Test::work()
{

    emit status(QVariant(QString("PASSED")));

    //    emit status(QVariant(QString("FAILED")));

}


void Test:: addValues(int id, QVariant surgeon_id, int dia_power_max,int dia_power_mode,int cortex_asp_max,int cortex_asp_mode , int  cortex_vac_max , int cortex_vac_mode , int polish_asp_max , int polish_asp_mode , int polish_vac_max ,int polish_vac_mode , int vit_cutrate_max , int vit_cutrate_mode , int vit_asp_max , int vit_asp_mode , int vit_vac_max , int vit_vac_mode ,int fs_top_left , int fs_top_right ,int fs_bottom_left ,int fs_bottom_right){
   QSqlQuery qry;
    qry.prepare("INSERT INTO doctor_info("
                "Id	,"
                "doctor_id	,"
                "dia_power_max	"
                "dial_power_mode	,"
                "cortex_asp_max,"
                "cortex_asp_mode,"
                "cortex_vac_max	,"
                "cortex_vac_mode,"
                "polish_asp_max,"
                "polish_asp_mode,"
                "polish_vac_max,"
                "polish_vac_mode,"
                "vit_cutrate_max,"
                "vit_cutrate_mode,"
                "vit_asp_max,"
                "vit_asp_mode,"
                "vit_vac_max,"
                "vit_vac_mode,"
                "fs_top_left,"
                "fs_top_right,"
                "fs_bottom_left,"
                "fs_bottom_right)"
                "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");

    qry.addBindValue(id);
    qry.addBindValue(surgeon_id);
    qry.addBindValue(dia_power_max);
    qry.addBindValue(dia_power_mode);
    qry.addBindValue(cortex_asp_max);
    qry.addBindValue(cortex_asp_mode);
    qry.addBindValue(cortex_vac_max);
    qry.addBindValue(cortex_vac_mode);
    qry.addBindValue(polish_asp_max);
    qry.addBindValue(polish_asp_mode);
    qry.addBindValue(polish_vac_max);
    qry.addBindValue(polish_vac_mode);
    qry.addBindValue(vit_cutrate_max);
    qry.addBindValue(vit_cutrate_mode);
    qry.addBindValue(vit_asp_max);
    qry.addBindValue(vit_asp_mode);
    qry.addBindValue(vit_vac_max);
    qry.addBindValue(vit_vac_mode);
    qry.addBindValue(fs_top_left);
    qry.addBindValue(fs_top_right);
    qry.addBindValue(fs_bottom_left);
    qry.addBindValue(fs_bottom_right);

     if(!qry.exec())qDebug()<<"error in entring";
}


void Test::createdatabase()
{
    qDebug()<<"start";

    QSqlDatabase db;
    db=QSqlDatabase::addDatabase("QSQLITE");
    db.setDatabaseName("/home/rahul/Desktop/qtprojects/vitrec/base");
    if(!db.open())qDebug()<<"problem";
    QString query="CREATE TABLE doctor_info("
                "Id	INTEGER PRIMARY KEY AUTOINCREMENT,"
                "doctor_id	TEXT,"
                "dia_power_max	INTEGER,"
                "dial_power_mode	INTEGER DEFAULT 0,"
                "cortex_asp_max	INTEGER,"
                "cortex_asp_mode	INTEGER DEFAULT 0,"
                "cortex_vac_max	INTEGER,"
                "cortex_vac_mode	INTEGER DEFAULT 0,"
                "polish_asp_max	INTEGER,"
                "polish_asp_mode	INTEGER DEFAULT 0,"
                "polish_vac_max	INTEGER,"
                "polish_vac_mode	INTEGER DEFAULT 0,"
                "vit_cutrate_max	INTEGER,"
                "vit_cutrate_mode	INTEGER DEFAULT 0,"
                "vit_asp_max	INTEGER,"
                "vit_asp_mode	INTEGER DEFAULT 0,"
                "vit_vac_max	INTEGER,"
                "vit_vac_mode	INTEGER DEFAULT 0,"
                "fs_top_left	INTEGER,"
                "fs_top_right	INTEGER,"
                "fs_bottom_left	INTEGER,"
                "fs_bottom_right	INTEGER)";

    QSqlQuery qry;
    if(!qry.exec(query))qDebug()<<"error in table creation";
    db.close();
    qDebug()<<"STOP";
}
