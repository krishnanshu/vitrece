import QtQuick 2.12
import QtQuick.Controls 2.5
import CustomControls 1.0

Page {
    width: 640
    height: 440

    Rectangle {
        id: rectangle
        x: 220
        y: 33
        width: 194
        height: 167
        color: "#ffffff"

        Column {
            id: column
            anchors.fill: parent
            spacing: 6

            Rectangle {
                id: rectangle3
                width: 190
                height: 120
                color: "#ffffff"
                RadialBar{
                    anchors.centerIn: parent
                    width: 170
                    height: 157
                    penStyle: Qt.RoundCap
                    dialType: RadialBar.MinToMax
                    progressColor: "green"
                    foregroundColor: "#191a2f"
                    backgroundColor: "light grey"
                    dialWidth: 15
                    startAngle: 50
                    spanAngle: 260
                    minValue: 0
                    maxValue: 100
                    value: number
                    textFont {
                        family: "Encode Sans"
                        italic: false
                        pointSize: 20
                    }
                }
            }

            Rectangle {
                id: rectangle4
                width: 190
                height: 36
                color: "#ffffff"

                Row {
                    id: row
                    anchors.fill: parent
                    spacing: 13

                    Rectangle {
                        id: rectangle6
                        width: 50
                        height: 34
                        color: "#ffffff"

                        Text {
                            id: element
                            text: qsTr("+")
                            anchors.fill: parent
                            font.pixelSize: 30
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.bold: true
                        }
                    }

                    Rectangle {
                        id: rectangle7
                        width: 55
                        height: 34
                        color: "#ffffff"

                        TextField {
                            id: textField
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            font.pointSize: 16
                            placeholderText: qsTr("00")
                        }
                    }

                    Rectangle {
                        id: rectangle8
                        width: 50
                        height: 34
                        color: "#ffffff"

                        Text {
                            id: element1
                            text: qsTr("-")
                            anchors.fill: parent
                            font.pixelSize: 30
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.bold: true
                        }
                    }
                }
            }
        }


    }

    Rectangle {
        id: rectangle1
        x: 90
        y: 227
        width: 193
        height: 186
        color: "#ffffff"
        Column {
            id: column0
            anchors.fill: parent
            spacing: 6

            Rectangle {
                id: rectangle03
                width: 190
                height: 143
                color: "#ffffff"
                RadialBar{
                    anchors.centerIn: parent
                    width: 180
                    height: 200
                    penStyle: Qt.RoundCap
                    dialType: RadialBar.MinToMax
                    progressColor: "blue"
                    foregroundColor: "#191a2f"
                    backgroundColor: "light grey"
                    dialWidth: 15
                    startAngle: 50
                    spanAngle: 260
                    minValue: 0
                    maxValue: 100
                    value: number
                    textFont {
                        family: "Encode Sans"
                        italic: false
                        pointSize: 20
                    }
                }
            }

            Rectangle {
                id: rectangle04
                width: 190
                height: 36
                color: "#ffffff"

                Row {
                    id: row0
                    anchors.fill: parent
                    spacing: 13

                    Rectangle {
                        id: rectangle06
                        width: 50
                        height: 34
                        color: "#ffffff"

                        Text {
                            id: element0
                            text: qsTr("+")
                            anchors.fill: parent
                            font.pixelSize: 30
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.bold: true
                        }
                    }

                    Rectangle {
                        id: rectangle07
                        width: 55
                        height: 34
                        color: "#ffffff"

                        TextField {
                            id: textField0
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            font.pointSize: 16
                            placeholderText: qsTr("00")
                        }
                    }

                    Rectangle {
                        id: rectangle08
                        width: 50
                        height: 34
                        color: "#ffffff"

                        Text {
                            id: element01
                            text: qsTr("-")
                            anchors.fill: parent
                            font.pixelSize: 30
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.bold: true
                        }
                    }
                }
            }
        }


    }

    Rectangle {
        id: rectangle2
        x: 340
        y: 220
        width: 200
        height: 200
        color: "#ffffff"
        Column {
            id: column00
            anchors.fill: parent
            spacing: 6

            Rectangle {
                id: rectangle003
                width: 190
                height: 143
                color: "#ffffff"
                RadialBar{
                    anchors.centerIn: parent
                    width: 180
                    height: 200
                    penStyle: Qt.RoundCap
                    dialType: RadialBar.MinToMax
                    progressColor: "blue"
                    foregroundColor: "#191a2f"
                    backgroundColor: "light grey"
                    dialWidth: 15
                    startAngle: 50
                    spanAngle: 260
                    minValue: 0
                    maxValue: 100
                    value: number
                    textFont {
                        family: "Encode Sans"
                        italic: false
                        pointSize: 20
                    }
                }
            }

            Rectangle {
                id: rectangle004
                width: 190
                height: 36
                color: "#ffffff"

                Row {
                    id: row00
                    anchors.fill: parent
                    spacing: 13

                    Rectangle {
                        id: rectangle006
                        width: 50
                        height: 34
                        color: "#ffffff"

                        Text {
                            id: element00
                            text: qsTr("+")
                            anchors.fill: parent
                            font.pixelSize: 30
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.bold: true
                        }
                    }

                    Rectangle {
                        id: rectangle007
                        width: 55
                        height: 34
                        color: "#ffffff"

                        TextField {
                            id: textField00
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            font.pointSize: 16
                            placeholderText: qsTr("00")
                        }
                    }

                    Rectangle {
                        id: rectangle008
                        width: 50
                        height: 34
                        color: "#ffffff"

                        Text {
                            id: element001
                            text: qsTr("-")
                            anchors.fill: parent
                            font.pixelSize: 30
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.bold: true
                        }
                    }
                }
            }
        }

    }


}

/*##^##
Designer {
    D{i:6}D{i:2}
}
##^##*/
