import QtQuick 2.12
import QtQuick.Controls 2.5
import CustomControls 1.0
Page {
    width: 640
    height: 440
    
    
    Rectangle {
        id: rectangle1
        x: 34
        y: 53
        width: 544
        height: 37
        color: "#ffffff"
        
        Row{
            id: row
            anchors.fill: parent
            anchors.rightMargin: -8
            anchors.bottomMargin: -1
            anchors.leftMargin: 8
            anchors.topMargin: -1
            spacing: 8

            Rectangle {
                id: rectangle
                width: 102
                height: 32
                color: "#ffffff"

                Text {
                    id: element
                    text: qsTr("Surgeon :")
                    anchors.fill: parent
                    font.pixelSize: 17
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    anchors.rightMargin: 0
                    anchors.bottomMargin: 0
                    anchors.leftMargin: 0
                    anchors.topMargin: 0
                    font.bold: true


                }
            }

            Rectangle {
                id: rectangle2
                width: 138
                height: 32
                color: "#ffffff"

                TextField {
                    id: textField
                    anchors.fill: parent
                    horizontalAlignment: Text.AlignHCenter
                    font.pointSize: 16
                    font.italic: true
                    placeholderText: qsTr("Text Field")
                }
            }

            Rectangle {
                id: rectangle3
                width: 47
                height: 40
                color: "#ffffff"

                Text {
                    id: element1
                    text: qsTr("ID")
                    anchors.fill: parent
                    font.pixelSize: 17
                    horizontalAlignment: Text.AlignHCenter
                    verticalAlignment: Text.AlignVCenter
                    font.bold: true
                }
            }




            
            
            
            
            
        }

        Rectangle {
            id: rectangle4
            x: 327
            y: 0
            width: 200
            height: 40
            color: "#ffffff"

            TextField {
                id: textField1
                horizontalAlignment: Text.AlignHCenter
                font.italic: true
                font.pointSize: 16
                placeholderText: qsTr("Text Field")
            }
        }
        
        
    }

    Rectangle {
        id: rectangle5
        x: 55
        y: 120
        width: 200
        height: 90
        color: "#ffffff"

        Column {
            id: column
            x: 20
            height: 90
            anchors.fill: parent
            spacing: 10

            Text {
                id: element2
                x: 10
                y: 10
                width: 170
                height: 35
                text: "Diathermy"
                font.pixelSize: 25
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.bold: true
            }

            TextField {
                id: textField2
                x: 40
                width: 100
                height: 36
                horizontalAlignment: Text.AlignHCenter
                font.pointSize: 16
                placeholderText: qsTr("00")
            }
        }

    }

    Rectangle {
        id: rectangle6
        x: 362
        y: 120
        width: 200
        height: 90
        color: "#ffffff"

        Column {
            id: column1
            anchors.fill: parent
            spacing: 10

            Text {
                id: element3
                x: 10
                y: 10
                width: 170
                height: 35
                text: qsTr("Polish")
                font.pixelSize: 24
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.bold: true
            }

            Row {
                id: row1
                y: 40
                width: 200
                height: 45
                spacing: 22

                Rectangle {
                    id: rectangle9
                    width: 50
                    height: 43
                    color: "#ffffff"

                    TextField {
                        id: textField3
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                        font.pointSize: 16
                        placeholderText: qsTr("00")
                    }
                }

                Rectangle {
                    id: rectangle10
                    width: 50
                    height: 43
                    color: "#ffffff"

                    TextField {
                        id: textField4
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                        font.pointSize: 16
                        placeholderText: qsTr("00")
                    }
                }

                Rectangle {
                    id: rectangle11
                    width: 50
                    height: 43
                    color: "#ffffff"

                    TextField {
                        id: textField5
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                        font.pointSize: 16
                        placeholderText: qsTr("00")
                    }
                }
            }


        }
    }

    Rectangle {
        id: rectangle7
        x: 55
        y: 247
        width: 200
        height: 101
        color: "#ffffff"

        Column {
            id: column2
            anchors.fill: parent
            spacing: 10

            Text {
                id: element8
                x: 10
                y: 10
                width: 170
                height: 35
                text: qsTr("Context")
                font.pixelSize: 24
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.bold: true
            }

            Row {
                id: row3
                y: 40
                width: 200
                height: 45
                spacing: 22

                Rectangle {
                    id: rectangle15
                    width: 50
                    height: 43
                    color: "#ffffff"

                    TextField {
                        id: textField6
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                        font.pointSize: 16
                        placeholderText: qsTr("00")
                    }
                }

                Rectangle {
                    id: rectangle16
                    width: 50
                    height: 43
                    color: "#ffffff"

                    TextField {
                        id: textField7
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                        font.pointSize: 16
                        placeholderText: qsTr("00")
                    }
                }

                Rectangle {
                    id: rectangle17
                    width: 50
                    height: 43
                    color: "#ffffff"

                    TextField {
                        id: textField8
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                        font.pointSize: 16
                        placeholderText: qsTr("00")
                    }
                }
            }

        }
    }

    Rectangle {
        id: rectangle8
        x: 362
        y: 247
        width: 200
        height: 101
        color: "#ffffff"

        Column {
            id: column3
            anchors.fill: parent
            spacing: 10

            Text {
                id: element4
                x: 10
                y: 10
                width: 170
                height: 35
                text: qsTr("vetrecetomy")
                font.pixelSize: 24
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.bold: true
            }

            Row {
                id: row2
                y: 40
                width: 200
                height: 45
                spacing: 22

                Rectangle {
                    id: rectangle14
                    width: 50
                    height: 43
                    color: "#ffffff"

                    TextField {
                        id: textField9
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                        font.pointSize: 16
                        placeholderText: qsTr("00")
                    }
                }

                Rectangle {
                    id: rectangle12
                    width: 50
                    height: 43
                    color: "#ffffff"

                    TextField {
                        id: textField10
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                        font.pointSize: 16
                        placeholderText: qsTr("00")
                    }
                }

                Rectangle {
                    id: rectangle13
                    width: 50
                    height: 43
                    color: "#ffffff"

                    TextField {
                        id: textField11
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                        font.pointSize: 16
                        placeholderText: qsTr("00")
                    }
                }
            }

        }
    }

    Rectangle {
        id: rectangle18
        x: 438
        y: 354
        width: 124
        height: 38
        color: "#729fcf"
        MouseArea{
            anchors.fill: parent

            Text {
                id: element5
                color: "#000000"
                text: qsTr("SAVE")
                anchors.fill: parent
                font.pixelSize: 24
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.italic: false
                font.bold: true
                styleColor: "#0f69ca"
            }
            onPressed: {
                parent.color = "dark blue"
            }
            onReleased: {
                parent.color = "#729fcf"
            }
        }
    }



    
    
}




/*##^##
Designer {
    D{i:12}D{i:18}D{i:16}D{i:26}D{i:36}
}
##^##*/
