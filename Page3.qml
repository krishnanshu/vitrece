import QtQuick 2.12
import QtQuick.Controls 2.5
import CustomControls 1.0
Page {
    width: 600
    height: 400

    Rectangle {
        id: rectangle
        x: 133
        y: 19
        width: 177
        height: 178
        color: "#ffffff"
        Column{
            y: 0
            anchors.fill: parent
            spacing: 5
            anchors.rightMargin: 0
            anchors.bottomMargin: -8
            anchors.leftMargin: 0
            anchors.topMargin: 8

            Rectangle {
                id: rectangle4
                x: 0
                width: 170
                height: 140
                color: "#ffffff"
                RadialBar{
                    anchors.centerIn: parent
                    width: 170
                    height: 170
                    penStyle: Qt.RoundCap
                    dialType: RadialBar.MinToMax
                    progressColor: "green"
                    foregroundColor: "#191a2f"
                    backgroundColor: "light grey"
                    dialWidth: 12
                    startAngle: 50
                    spanAngle: 260
                    minValue: 0
                    maxValue: 100
                    value: number
                    textFont {
                        family: "Encode Sans"
                        italic: false
                        pointSize: 20
                    }
                }
            }

            Row {
                id: row
                width: 174
                height: 34
                spacing: 6

                Rectangle {
                    id: rectangle5
                    width: 50
                    height: 30
                    color: "#ffffff"

                    Text {
                        id: element
                        text: qsTr("+")
                        anchors.fill: parent
                        font.pixelSize: 30
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        font.bold: true
                    }
                }

                Rectangle {
                    id: rectangle6
                    width: 60
                    height: 30
                    color: "#ffffff"

                    TextField {
                        id: textField
                        width: 525
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                        font.bold: false
                        font.pointSize: 17
                        placeholderText: qsTr("00")
                    }
                }

                Rectangle {
                    id: rectangle7
                    width: 50
                    height: 30
                    color: "#ffffff"

                    Text {
                        id: element1
                        text: qsTr("-")
                        anchors.fill: parent
                        font.pixelSize: 30
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        font.bold: true
                    }
                }
            }


        }
    }

    Rectangle {
        id: rectangle1
        x: 316
        y: 19
        width: 177
        height: 178
        color: "#ffffff"

        Column {
            id: column
            x: 0
            y: 8
            width: 177
            height: 178
            spacing: 5

            Rectangle {
                id: rectangle8
                width: 170
                height: 130
                color: "#ffffff"
                RadialBar{
                    anchors.centerIn: parent
                    width: 170
                    height: 162
                    penStyle: Qt.RoundCap
                    dialType: RadialBar.MinToMax
                    progressColor: "green"
                    foregroundColor: "#191a2f"
                    backgroundColor: "light grey"
                    dialWidth: 12
                    startAngle: 50
                    spanAngle: 260
                    minValue: 0
                    maxValue: 100
                    value: number
                    textFont {
                        family: "Encode Sans"
                        italic: false
                        pointSize: 20
                    }
                }


            }

            Row {
                id: row1
                width: 174
                height: 34
                spacing: 6

                Rectangle {
                    id: rectangle9
                    width: 50
                    height: 30
                    color: "#ffffff"

                    Text {
                        id: element2
                        text: qsTr("+")
                        anchors.fill: parent
                        font.pixelSize: 30
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        font.bold: true
                    }
                }

                Rectangle {
                    id: rectangle10
                    width: 60
                    height: 30
                    color: "#ffffff"

                    TextField {
                        id: textField1
                        anchors.fill: parent
                        horizontalAlignment: Text.AlignHCenter
                        font.pointSize: 17
                        placeholderText: qsTr("00")
                    }
                }

                Rectangle {
                    id: rectangle11
                    width: 50
                    height: 30
                    color: "#ffffff"

                    Text {
                        id: element3
                        text: qsTr("-")
                        anchors.fill: parent
                        font.pixelSize: 30
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        font.bold: true
                    }
                }
            }



        }

    }

    Rectangle {
        id: rectangle2
        x: 23
        y: 225
        width: 178
        height: 158
        color: "#ffffff"

        Column {
            id: column1
            width: 200
            anchors.fill: parent
            anchors.leftMargin: 77
            anchors.rightMargin: -80
            anchors.topMargin: 23
            anchors.bottomMargin: -52
            spacing: 6

            Rectangle {
                id: rectangle12
                width: 180
                height: 140
                color: "#ffffff"
                RadialBar{
                    anchors.centerIn: parent
                    width: 200
                    height: 185
                    penStyle: Qt.RoundCap
                    dialType: RadialBar.MinToMax
                    progressColor: "red"
                    foregroundColor: "#191a2f"
                    backgroundColor: "light grey"
                    dialWidth: 20
                    startAngle: 50
                    spanAngle: 260
                    minValue: 0
                    maxValue: 100
                    value: number
                    textFont {
                        family: "Encode Sans"
                        italic: false
                        pointSize: 20
                    }
                }

            }

            Rectangle {
                id: rectangle13
                width: 190
                height: 30
                color: "#ffffff"

                Row {
                    id: row2
                    anchors.fill: parent
                    anchors.leftMargin: 0
                    anchors.rightMargin: 0
                    anchors.bottomMargin: 0
                    anchors.topMargin: 0
                    spacing: 10

                    Rectangle {
                        id: rectangle14
                        width: 50
                        height: 30
                        color: "#ffffff"

                        Text {
                            id: element4
                            text: qsTr("+")
                            anchors.fill: parent
                            font.pixelSize: 30
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.bold: true
                        }
                    }

                    Rectangle {
                        id: rectangle15
                        width: 55
                        height: 30
                        color: "#ffffff"

                        TextField {
                            id: textField2
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            font.pointSize: 16
                            placeholderText: qsTr("00")
                        }
                    }

                    Rectangle {
                        id: rectangle16
                        width: 50
                        height: 30
                        color: "#ffffff"

                        Text {
                            id: element5
                            text: qsTr("-")
                            anchors.fill: parent
                            font.pixelSize: 30
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.bold: true
                        }
                    }
                }
            }
        }


    }

    Rectangle {
        id: rectangle3
        x: 316
        y: 225
        width: 197
        height: 200

        Column {
            id: column2
            anchors.fill: parent
            spacing: 6

            Rectangle {
                id: rectangle17
                y: 10
                width: 180
                height: 160
                color: "#ffffff"
                RadialBar{
                    anchors.centerIn: parent
                    width: 200
                    height: 185
                    penStyle: Qt.RoundCap
                    dialType: RadialBar.MinToMax
                    progressColor: "red"
                    foregroundColor: "#191a2f"
                    backgroundColor: "light grey"
                    dialWidth: 20
                    startAngle: 50
                    spanAngle: 260
                    minValue: 0
                    maxValue: 100
                    value: number
                    textFont {
                        family: "Encode Sans"
                        italic: false
                        pointSize: 20
                    }
                }

            }

            Rectangle {
                id: rectangle18
                width: 190
                height: 30
                color: "#ffffff"

                Row {
                    id: row3
                    x: 0
                    y: 0
                    width: 195
                    height: 35
                    spacing: 10

                    Rectangle {
                        id: rectangle19
                        width: 50
                        height: 30
                        color: "#ffffff"

                        Text {
                            id: element6
                            text: qsTr("+")
                            anchors.fill: parent
                            font.pixelSize: 30
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.bold: true
                        }
                    }

                    Rectangle {
                        id: rectangle20
                        x: 65
                        width: 53
                        height: 30
                        color: "#ffffff"

                        TextField {
                            id: textField3
                            anchors.fill: parent
                            horizontalAlignment: Text.AlignHCenter
                            font.pointSize: 16
                            placeholderText: qsTr("00")
                        }
                    }

                    Rectangle {
                        id: rectangle21
                        x: 132
                        width: 50
                        height: 30
                        color: "#ffffff"

                        Text {
                            id: element7
                            text: qsTr("-")
                            anchors.fill: parent
                            font.pixelSize: 30
                            horizontalAlignment: Text.AlignHCenter
                            verticalAlignment: Text.AlignVCenter
                            font.bold: true
                        }
                    }








                }
            }
        }


    }


}

/*##^##
Designer {
    D{i:3}D{i:28}D{i:24}D{i:36}
}
##^##*/
