import QtQuick 2.12
import QtQuick.Controls 2.5
import CustomControls 1.0
Page {
    property int number: if(textField.text >= 0 && textField.text<= 100)
                             number : textField.text
    width: 640
    height: 440
    Rectangle{
        id : rect1
        x: 160
        y: 121
        width: 362
        height: 307

        Column {
            id: column
            anchors.fill: parent
            anchors.rightMargin: 0
            anchors.topMargin: -41
            anchors.bottomMargin: 41
            anchors.leftMargin: 0
            spacing: 10
        }

        Rectangle {
            id: rectangle
            x: 74
            y: 246
            width: 187
            height: 61
            color: "#ffffff"

            Row {
                id: row
                anchors.fill: parent
                anchors.leftMargin: 4
                anchors.bottomMargin: 8
                anchors.topMargin: -9
                anchors.rightMargin: -4
                spacing: 11

                Rectangle {
                    id: rectangle2
                    width: 51
                    height: 50
                    color: "#ffffff"
                    MouseArea{
                        anchors.fill : parent
                        onPressed: textField.text++
                    }

                    Text {
                        id: name
                        x: 8
                        y: 14
                        width: 27
                        height: 28
                        text: qsTr("+")
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        font.pointSize: 20
                    }

                }
                Rectangle {
                    id: rectangle3
                    width: 51
                    height: 50
                    color: "#ffffff"

                    TextField {
                        anchors.fill: parent
                        antialiasing: true
                        horizontalAlignment: Text.AlignHCenter
                        id: textField

                    }
                }
                Rectangle {
                    id: rectangle4
                    width: 51
                    height: 50
                    color: "#ffffff"
                    MouseArea{
                        anchors.fill : parent
                        onPressed:  textField.text--
                    }

                    Text {
                        id: name1
                        x: 12
                        y: 14
                        width: 31
                        height: 28
                        text: qsTr("-")
                        horizontalAlignment: Text.AlignHCenter
                        verticalAlignment: Text.AlignVCenter
                        font.bold: true
                        font.pointSize: 20
                    }
                }
            }
        }

        Rectangle {
            id: rectangle1
            x: 81
            y: 0
            width: 242
            height: 206
            color: "#ffffff"
            anchors.horizontalCenterOffset: -13
            anchors.horizontalCenter: parent.horizontalCenter
            RadialBar{
                anchors.centerIn: parent
                width: 300
                height: 300
                penStyle: Qt.RoundCap
                dialType: RadialBar.MinToMax
                progressColor: "#1e5dc6"
                foregroundColor: "#191a2f"
                backgroundColor: "light grey"
                dialWidth: 30
                startAngle: 50
                spanAngle: 260
                minValue: 0
                maxValue: 100
                value: number
                textFont {
                    family: "Encode Sans"
                    italic: false
                    pointSize: 20
                }
            }
        }


    }
}


