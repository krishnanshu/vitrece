import QtQuick 2.12
import QtQuick.Controls 2.5
import CustomControls 1.0

ApplicationWindow {
    id: window
    width: 640
    height: 480
    visible: true
    title: qsTr("Stack")


    Rectangle {

        id: rectangle
        color: "green"
        anchors.rightMargin: 0
        anchors.leftMargin: 0
        anchors.topMargin: -41
        anchors.bottomMargin: 0
        radius: height/3;
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: parent.bottom
        anchors.bottom: parent.bottom

        Row {

            id: row
            y: 0
            height: 41
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 0
            anchors.rightMargin: 90
            anchors.leftMargin: 119
            spacing: 10

            Rectangle {
                id: button
                width: 65
                height:41
                radius: height/4
                transformOrigin: Item.Center
                Text {
                    anchors.centerIn:   parent
                    id: text1
                    text: qsTr("button1")
                }
                MouseArea{
                    anchors.fill: parent
                    onPressed:{

                        parent.color = "red"
                        stackView.push("Home.qml")
                    }
                    onReleased: parent.color = "white"
                }
            }

            Rectangle {
                id: button1
                width: 65
                height:41
                radius: height/4
                transformOrigin: Item.Center
                Text {
                    anchors.centerIn:   parent
                    id: text2
                    text: qsTr("button2")
                }
                MouseArea{
                    anchors.fill: parent
                    onPressed:{

                        parent.color = "red"
                        stackView.push("Homeview.qml")
                    }
                    onReleased: parent.color = "white"
                }
            }


            Rectangle {
                id: button3
                width: 65
                height:41
                radius: height/4
                transformOrigin: Item.Center
                Text {
                    anchors.centerIn:   parent
                    id: text3
                    text: qsTr("button3")
                }
                MouseArea{
                    anchors.fill: parent
                    onPressed:{

                        parent.color = "red"
                        stackView.push("Page3.qml")
                    }
                    onReleased: parent.color = "white"
                }
            }

            Rectangle {
                id: button4
                width: 65
                height:41
                radius: height/4
                transformOrigin: Item.Center
                Text {
                    anchors.centerIn:   parent
                    id: text4
                    text: qsTr("button4")
                }
                MouseArea{
                    anchors.fill: parent
                    onPressed:{

                        parent.color = "red"
                        stackView.push("doctor.qml")
                    }
                    onReleased: parent.color = "white"
                }
            }
        }
    }

    StackView {
        id: stackView
        initialItem: "Home.qml"
    }

    Rectangle {
        id: rectangle1
        x: 533
        y: 0
        width: 107
        height: 36
        color: "#ffffff"

        Rectangle {
            id: rectangle2
            x: 7
            y: 3
            width: 34
            height: 30
            color: "#729fcf"
            Text {
                anchors.centerIn: parent
                id: name2
                text: qsTr("<")
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pointSize: 15
                font.bold: true
            }
        }

        Rectangle {
            id: rectangle3
            x: 62
            y: 3
            width: 34
            height: 30
            color: "#729fcf"
            MouseArea{
                anchors.fill: rectangle3
                onPressed:  {
                    parent.color = "red"

                }
                onReleased: parent.color = "#729fcf"
            }

            Text {
                anchors.centerIn: parent
                id: name3
                text: qsTr(">")
                horizontalAlignment: Text.AlignHCenter
                verticalAlignment: Text.AlignVCenter
                font.pointSize: 15
                font.bold: true
            }
        }
    }


}


